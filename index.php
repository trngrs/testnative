<?php

include "Controllers/AuthorsController.php";
include "Controllers/HeadingController.php";
include "Controllers/NewsController.php";

use \Controllers\AuthorsController;
use \Controllers\HeadingController;
use Controllers\NewsController;

class Index
{
    protected $authorsController = [];
    protected $headingController = [];
    protected $newsController = [];


    public function __construct(){
        $this->authorsController = new AuthorsController();
        $this->headingController = new HeadingController();
        $this->newsController = new NewsController();
    }

    public function route()
    {
        $request_url = rtrim(ltrim(urldecode(parse_url($_SERVER['REQUEST_URI'], 5)), '/'), '/');
        $params = array_filter(explode("/", $request_url));

        if (count($params) < 2){
            $routes = [
                '' => 'views/index.php',
                'author' => 'views/author.php',
                'news' => 'views/news.php',
            ];
            if (isset($routes[$request_url])){
                require_once $routes[$request_url];
            } else {
                require_once('views/404.php');
            }
        }

        if (count($params) >= 2){
            if ($params[0] == 'api'){
                $routes = [
                    'authors' => $this->authorsController->index(),
                    'authors-show' => $this->authorsController->show(),
                    'headings' => $this->headingController->index(),
                    'news' => $this->newsController->index(),
                    'news-show' => $this->newsController->show(),
                ];
                header('Content-Type: application/json; charset=UTF-8');
                print_r(json_encode($routes[$params[1]]));
            }
        }

    }
}
$routes = new Index;
$routes->route();