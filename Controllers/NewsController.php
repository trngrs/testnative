<?php

namespace Controllers;

include_once "Models/News.php";
use \Models\News;

class NewsController
{

    public function index()
    {
        $author_id = null;
        $heading_id = null;
        if (isset($_GET["author_id"]) && $_GET["author_id"]){
            $author_id = $_GET["author_id"];
        } elseif (isset($_GET["heading_id"]) && $_GET["heading_id"]){
            $heading_id = $_GET["heading_id"];
        }

        $query = [
            'author_id' => $author_id,
            'heading_id' => $heading_id,
        ];

        $author = new News();

        return $author->get($query);
    }


    public function show()
    {
        $news_id = null;
        if (isset($_GET["id"]) && $_GET["id"]){
            $news_id = $_GET["id"];
        }

        $author = new News();

        return $author->find($news_id);
    }
}