<?php

namespace Controllers;

include "Models/Author.php";
use \Models\Author;

class AuthorsController
{

    public function index()
    {
        $author = new Author();

        return $author->get();
    }

    public function show()
    {
        $author_id = null;
        if (isset($_GET["author_id"]) && $_GET["author_id"]){
            $author_id = $_GET["author_id"];
        }

        $author = new Author();

        return $author->find($author_id);
    }
}