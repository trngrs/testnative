<?php

namespace Controllers;

include_once "Models/Heading.php";

use \Models\Heading;

class HeadingController
{

    public function index()
    {
        $heading = new Heading();

        $res = $heading->get($parent = 0);
        $data = [];
        foreach ($res as $item) {
            $data[$item['id']] = $item;
            if ($res = $heading->get($item['id'])) {
                $row = $heading->get($item['id']);
                foreach ($row as $childItem) {
                    $data[$item['id']]['child'][$childItem['id']] = $childItem;
                    if ($res = $heading->get($childItem['id'])) {
                        foreach ($res as $sub2) {
                            $data[$item['id']]['child'][$childItem['id']]['sub'][$sub2['id']] = $sub2;
                        }
                    }
                }
            }
        }

        return $data;
    }
}