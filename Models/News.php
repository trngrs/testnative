<?php

namespace Models;
include_once "ConnectDB.php";

use ConnectDB;

class News extends ConnectDB
{
    protected $DB;
    protected string $table = 'news';

    public function __construct()
    {
        $connectDB = new ConnectDB();
        $this->DB = $connectDB->connect();
    }

    public function get(array $query)
    {
        $sql = "SELECT news.*, authors.name, authors.img, authors.signature FROM $this->table
            LEFT JOIN authors ON $this->table.author_id = authors.id ";

        if ($query['heading_id']) {
            $sql .= "
            LEFT JOIN news_heading ON $this->table.id = news_heading.news_id
            where news_heading.news_id = " . $query['heading_id'];
        } elseif ($query['author_id']) {
            $sql .= " where $this->table.author_id = " . $query['author_id'];
        }

        $res = $this->DB->query($sql);

        return $this->format($res);
    }

    public function find($id)
    {
        $res = $this->DB->query("SELECT news.*, authors.name, authors.img, authors.signature  FROM $this->table
            LEFT JOIN authors ON $this->table.author_id = authors.id
            where news.id = $id");

        return $this->format($res);
    }


    public function format($res)
    {
        $data = [];
        if ($res !== false && $res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {
                $data[] = [
                    'id' => $row['id'],
                    'title' => $row['title'],
                    'announcement' => $row['announcement'],
                    'text' => $row['text'],
                    'author_id' => $row['author_id'],
                    'author_name' => $row['name'],
                    'author_img' => $row['img'],
                    'author_signature' => $row['signature'],
                ];
            }
        }
        return $data;
    }

    function __destruct()
    {
        $this->DB->close();
    }
}