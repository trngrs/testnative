<?php

namespace Models;

include_once "ConnectDB.php";
use ConnectDB;


class Heading extends ConnectDB
{
    protected $DB;
    protected $table = 'heading';

    public function __construct()
    {
        $connectDB = new ConnectDB();
        $this->DB = $connectDB->connect();
    }

    public function get($parent){
        $res = $this->DB->query("SELECT * FROM $this->table where parent = $parent");

        return $this->format($res);
    }


    public function format($res){
        $data = [];
        if ($res !== false && $res->num_rows > 0) {
            while($row = $res->fetch_assoc()) {
                $data[] = [
                    'id' => $row['id'],
                    'title' => $row['title'],
                    'parent' => $row['parent'],
                ];
            }
        }
        return $data;
    }

    function __destruct() {
        $this->DB->close();
    }
}