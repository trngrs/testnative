<?php

class ConnectDB
{
    public function connect()
    {
        $servername = "localhost";
        $username = "root";
        $password = "rootroot";
        $dbname = "test_db";

        try {
            return new mysqli($servername, $username, $password, $dbname);
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}