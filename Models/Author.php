<?php

namespace Models;
include_once "ConnectDB.php";

use ConnectDB;

class Author extends ConnectDB
{
    protected $DB;

    public function __construct()
    {
        $connectDB = new ConnectDB();
        $this->DB = $connectDB->connect();
    }

    public function get()
    {
        $res = $this->DB->query("SELECT * FROM authors");

        return $this->format($res);;
    }

    public function find($id)
    {
        $res = $this->DB->query("SELECT * FROM authors where id = $id");

        return $this->format($res);;
    }


    public function format($res)
    {
        $data = [];
        if ($res !== false && $res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {
                $data[] = [
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'img' => $row['img'],
                    'signature' => $row['signature'],
                ];
            }
        }
        return $data;
    }

    function __destruct()
    {
        $this->DB->close();
    }
}