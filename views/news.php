<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<body>
<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Заголовок</th>
        <th scope="col">Анонс</th>
        <th scope="col">Текст</th>
        <th scope="col">фио автора</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script>
    $( document ).ready(function() {
        $.ajax({
            type: "GET",
            url: '/api/news-show',
            data: {
                'id': <?php echo $_GET["id"] ?>,
            },
            success: function (res){
                res.forEach(elem =>
                    $("tbody").append(
                        "<tr>\n" +
                        "        <th scope=\"row\">" + elem.id + "</th>\n" +
                        "        <td>" + elem.title + "</td>\n" +
                        "        <td>" + elem.announcement + "</td>\n" +
                        "        <td>" + elem.text + "</td>\n" +
                        "        <td><a href='author?author_id=" + elem.id + "'>" + elem.author_name + "</a>" +
                        "</td>\n" +
                        "    </tr>"
                    )
                );

            }
        });
    });

</script>


</body>
</html>