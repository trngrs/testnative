<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <div class="row">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">news</a>
                </div>
                <ul class="nav navbar-nav" id="content">

                </ul>
            </div>
        </nav>

    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Заголовок</th>
                    <th scope="col">Анонс</th>
                    <th scope="col">Текст</th>
                    <th scope="col">фио автора</th>
                    <th scope="col">фильтр по автору</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script>
    $( document ).ready(function() {
        let heading_id = null;
        let author_id = null;

        function getNews() {
            $.ajax({
                type: "GET",
                url: '/api/news',
                data: {
                    'heading_id': heading_id,
                    'author_id': author_id,
                },
                success: function (res) {
                    $("tbody").html('');
                    res.forEach(elem =>

                        $("tbody").append(
                            "<tr>\n" +
                            "        <th scope=\"row\"><a href='news?id="+elem.id+"'>" + elem.id + "</a></th>\n" +
                            "        <td>" + elem.title + "</td>\n" +
                            "        <td>" + elem.announcement + "</td>\n" +
                            "        <td>" + elem.text + "</td>\n" +
                            "        <td><a href='author?author_id=" + elem.id + "'>" + elem.author_name + "</a>" +
                            "<td><span class='author' aid='"+elem.id+"'>" + elem.author_name +"</span></td>" +
                            "</td>\n" +
                            "    </tr>"
                        )
                    );

                    $(".author").on( "click", function () {
                        author_id = $(this).attr('aid')
                        getNews()
                    });
                }
            });
        }





        getNews();

        $.ajax({
            type: "GET",
            url: '/api/headings',
            success: function (res){
                for (const [key, value] of Object.entries(res)) {
                    let text_html = '';
                    if (value.child == undefined){
                        text_html+= "<li class=\"active\"><a href=\"#\" class='click' mid='"+value.id+"'>"+value.title+"</a></li>\n";
                    }else{
                        text_html += " <li class=\"dropdown\">\n" +
                            "    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" >"+value.title+"\n" +
                            "    <span class=\"caret\"></span></a>\n" +
                            "    <ul class=\"dropdown-menu\">\n";

                            for (const [key, sub1] of Object.entries(value.child)) {
                                if (sub1.sub == undefined) {
                                    text_html += "<li><a href=\"#\" class='click' mid='"+sub1.id+"'>" + sub1.title + "</a></li>\n";
                                }else{
                                    text_html += " <li class=\"dropdown1\">\n" +
                                    "<a class=\"dropdown1-toggle\" data-toggle=\"dropdown1\" href=\"#\">"+sub1.title+"\n" +
                                        "    <span class=\"caret\"></span></a>\n" +
                                        "    <ul class=\"dropdown1-menu\">";

                                        for (const [key, sub2] of Object.entries(sub1.sub)) {
                                            text_html += "<li><a href=\"#\" class='click' mid='"+sub2.id+"'>>" + sub2.title + "</a></li>\n";
                                        }

                                    text_html += "</ul></li>";
                                }

                            }

                        text_html += " </ul> </li>"
                    }

                    $( "#content" ).append(text_html);

                }
                $(".click").on( "click", function (){

                        heading_id= $(this).attr('mid')
                    getNews()
                });
            }
        });


    });




</script>


</body>
</html>